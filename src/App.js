import AttendanceLogin from "./pages/LoginPage";
import SuccessPage from "./pages/SuccessLoginPage";
import FailLoginPage from "./pages/FailLoginPage";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Redirect } from "react-router-dom";

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <Redirect
            to="/login-attendance/:token"
          />
        </Route>
        <Route
          exact
          path="/login-attendance/:token"
          component={AttendanceLogin}
        />
        <Route path="/success-attendance-page" component={SuccessPage} />
        <Route path="/failed-attendance-page" component={FailLoginPage} />
      </Switch>
    </Router>
  );
}

export default App;
