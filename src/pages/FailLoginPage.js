import { Box, Typography, Container } from "@mui/material";

export default function FailLoginPage() {

  return (
    <Container>
      <Box sx={{ maxWidth: 500, margin: "0 auto" }}>
          <Typography
            variant="h3"
            paragraph
            style={{ fontWeight: 700, color: "#212b36", lineHeight: 1.5 }}
          >
           Invalid Attendance Link!
          </Typography>

          <Box
          
            component="img"
            src="/images/error-attendance.png"
            sx={{ height: 500, marginLeft:"-35%"}}
          />
        </Box>
    </Container>
  );
}