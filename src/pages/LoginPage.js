import React, { useEffect, useState } from "react";
import { useParams, useHistory } from "react-router-dom";
import axios from "axios";
import Message from "../components/Message";
import { configEnv } from "../common/const";

export default function AttendanceLogin() {
  var { token } = useParams();
  const history = useHistory();
  console.log(token);
  const [success, setSuccess] = useState(false);
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [tokenUser, setTokenUser] = useState("");
  const [notify, setNotify] = useState({
    isOpen: false,
    message: "",
    type: "",
  });

  useEffect(() => {
    axios
      .get(
        configEnv.FETCH_STRING + `Attendance/ValidateAttendanceToken/${token}`
      )
      .then((response) => {
        if (response.data) {
          setSuccess(response.data);
        } else {
          history.push({
            pathname: "/failed-attendance-page",
          });
        }
      });
  }, [success]);

  console.log(success);

  useEffect(() => {
    if (success === true && tokenUser !== "") {
      axios
        .get(
          configEnv.FETCH_STRING +
            `Attendance/TakeAttendanceClass/${token}?tokenUser=${tokenUser}`
        )
        .then((response) => {
          history.push({
            pathname: "/success-attendance-page",
          });
        })
        .catch((err) => {
          setNotify({
            isOpen: true,
            message: "Error Attendance",
            type: "error",
          });
        });
    }
  }, [success, tokenUser]);

  const handleSubmit = () => {
    const data = {
      username: username,
      password: password,
    };
    axios
      .post(configEnv.FETCH_STRING + "Auth/Login", data)
      .then((response) => {
        console.log(response.data["token"]);
        setSuccess(true);
        setTokenUser(response.data["token"]);
      })
      .catch((err) => {
        setNotify({
          isOpen: true,
          message: "Login failed",
          type: "error",
        });
      });
  };

  return (
    <>
      <div
        className="card"
        style={{ display: "flex", maxWidth: "600px", margin: "auto" }}
      >
        <div
          className="card-header"
          style={{ backgroundColor: "#343a40", color: "#fff" }}
        >
          <h3 className="card-title">Login to atttendance</h3>
        </div>

        <div className="card-body">
          <div className="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input
              className="form-control"
              id="exampleInputEmail1"
              placeholder="Enter email"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
          </div>
          <div className="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input
              type="password"
              className="form-control"
              id="exampleInputPassword1"
              placeholder="Password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>{" "}
        </div>

        <div className="card-footer">
          <button className="btn btn-success" onClick={handleSubmit}>
            Login
          </button>
        </div>
      </div>
      <Message notify={notify} setNotify={setNotify} />
    </>
  );
}
